//
//  utilities.c
//  Padoru
//
//  Created by yumiistar on 11/18/18.
//  Copyright © 2018 yumiistar. All rights reserved.
//
//  libimobiledevice & libirecovery
//  Copyright © 2010-2013 libimobiledevice. All Rights Reserved.
//

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <Padoru.h>
#include <termios.h>

#define _FMT_lld "%lld"
#define baudrate "115200"

#include <libirecovery.h>
#include <libimobiledevice/lockdown.h>
#include <libimobiledevice/libimobiledevice.h>

int fd = -1;
int res = -1;
char* name = NULL;
idevice_t device = NULL;
static char *udid = NULL;
irecv_client_t client = NULL;
lockdownd_client_t lockdown = NULL;
idevice_error_t ret = IDEVICE_E_UNKNOWN_ERROR;
lockdownd_error_t ldret = LOCKDOWN_E_UNKNOWN_ERROR;

/********** Serial Terminal **********/
void set_speed(struct termios *config, speed_t speed) {
    cfsetispeed(config, speed);
    cfsetospeed(config, speed);
}

void tool_serial_terminal(int argc, char *argv[]) {
    struct termios conf;
    struct termios save;
    
    char buffer[1024];
    int reads;
    
    const char *port = argv[2];
    
    fd = open(port, O_RDWR | O_NONBLOCK);
    if (fd < 0) {
        printf("ERROR %s: %s\n", port, strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    if (tcgetattr(fd, &conf) != 0) {
        printf("%s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    conf.c_cflag   &= ~ CLOCAL;
    tcsetattr(fd, TCSANOW, &conf);
    
    // We manipulate fd to block it
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) &  ~O_NONBLOCK);
    
    printf("\n");
    
    printf("OK: %s: Opened\n", port);
    tcgetattr(fd, &conf);
    
    memcpy(&save, &conf, sizeof(struct termios));
    cfmakeraw(&conf);
    set_speed(&conf, B115200); // Set the Baud Rate at 115200
    
    printf("OK: %s: baudrate set to %s\n", port, baudrate);
    
    conf.c_cflag &= ~PARENB;
    conf.c_cflag &= ~CSIZE;
    conf.c_cflag |= CS8;
    conf.c_cflag &= ~CSTOPB;
    conf.c_cflag |= CRTSCTS;
    conf.c_cc[VMIN] = 1;
    conf.c_cc[VTIME] = 0;
    conf.c_cflag &= ~CLOCAL;
    conf.c_cflag |= HUPCL;
    
    if (tcsetattr(fd, TCSANOW, &conf) < 0) {
        printf("%s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    printf("OK: %s: Configured\n", port);
    
    printf("OK: Data Reception Started\n");
    
    while (1) {
        reads = read(fd, buffer, 1024);
            if (reads < 0)  {
            printf("%s", strerror(errno));
            exit(EXIT_FAILURE);
        }
        if (reads == 0)
            exit(EXIT_SUCCESS);
            write(STDOUT_FILENO, buffer, reads);
        }
    
    printf("OK: Reception Ended\n");
    
    printf("\n");
    
    fd = open(port, O_RDWR | O_NONBLOCK);
    save.c_cflag |= CLOCAL;
    tcsetattr(fd, TCSANOW, &save);
    close(fd);
    
    exit(EXIT_SUCCESS);
}
    
/********** iDevice Infos **********/
// Get iDevice UDID
static void tool_get_udid(void) {
    // Required to print UDID
    if (IDEVICE_E_SUCCESS != idevice_new(&device, NULL)) {
        // void lmao
    }

    ret = idevice_get_udid(device, &udid);
    if (IDEVICE_E_SUCCESS != idevice_new(&device, NULL)) {
        // printf("UDID: → Normal\n");
    } else {
        printf("UDID: %s\n", udid);
    }
}

static const char* mode_to_str(int mode) {
    switch (mode) {
        case IRECV_K_RECOVERY_MODE_1:
        case IRECV_K_RECOVERY_MODE_2:
        case IRECV_K_RECOVERY_MODE_3:
        case IRECV_K_RECOVERY_MODE_4:
            return "Recovery";
            exit(EXIT_FAILURE);
        case IRECV_K_DFU_MODE:
            return "DFU";
            exit(EXIT_FAILURE);
        case IRECV_K_WTF_MODE:
            return "WTF";
            exit(EXIT_FAILURE);
        default:
            return "Normal";
            exit(EXIT_FAILURE);
    }
}

static char* get_uuid(void) {
    int i = 0;
    const char *chars = "ABCDEF0123456789";
    char *uuid = (char*)malloc(sizeof(char) * 33);

    srand(time(NULL));

    for (i = 0; i < 32; i++) {

        uuid[i] = chars[rand() % 16];

    }

    uuid[32] = '\0';

    return uuid;
}

static void tool_get_uuid(void) {
    char *uuid = get_uuid(); // Requiered to print UUID
    if (IDEVICE_E_SUCCESS != idevice_new(&device, NULL)) {
        // void loul
    } else {
        printf("UUID: %s\n", uuid);
    }
}

static void tool_get_name(void) {
    lockdownd_error_t lerr = lockdownd_client_new_with_handshake(device, &lockdown, "idevicename");
    if (lerr != LOCKDOWN_E_SUCCESS) {
        idevice_free(device);
        // exit(EXIT_FAILURE);
    }

    lerr = lockdownd_get_device_name(lockdown, &name);
    if (IDEVICE_E_SUCCESS != idevice_new(&device, NULL)) {
        printf("NAME: → Normal\n");
    } else {
        if (name) {
            printf("NAME: %s\n", name);
            free(name);
            res = 0;
        }
    }
}

static void tool_get_all(void) {
    int i = 0;
    irecv_error_t error = 0;
    unsigned long long ecid = 0;

    irecv_init();
    
    for (i = 0; i <= 5; i++) {
        if (irecv_open_with_ecid(&client, ecid) != IRECV_E_SUCCESS)
            sleep(1);
        else
            break;

        if (i == 5) {
            exit(EXIT_FAILURE);
        }
    }

    int ret, mode;
    irecv_device_t device = NULL;
    irecv_devices_get_device_by_client(client, &device);
    const struct irecv_device_info *devinfo = irecv_get_device_info(client);

    ret = irecv_get_mode(client, &mode);
    if (ret == IRECV_E_SUCCESS) {
        fprintf(stdout, "MODE: %s\n", mode_to_str(mode));
    } else {
        fprintf(stderr, "MODE: N/A\n");
    }

    tool_get_name();

    // tool_get_udid(); // useless now 
        
    // tool_get_uuid(); // useless now
    
    if (devinfo) {
        fprintf(stdout, "BDID: 0x%02x\n", devinfo->bdid);
        fprintf(stdout, "ECID: " _FMT_lld "\n", devinfo->ecid);
        fprintf(stdout, "IMEI: %s\n", (devinfo->imei) ? devinfo->imei : "N/A");
        fprintf(stdout, "MODEL: %s\n", device->hardware_model);
        fprintf(stdout, "DEVICE: %s\n", device->product_type);
    } else {
        fprintf(stdout, "BDID: N/A\n", devinfo->bdid);
        fprintf(stdout, "ECID: N/A\n", devinfo->ecid);
        fprintf(stdout, "MODEL: N/A\n", device->hardware_model);
        fprintf(stdout, "DEVICE: N/A\n", device->product_type);
    }

    irecv_close(client);
    irecv_exit();
}

void tool_enter_recovery(void) {

    printf("\n");
    
    tool_get_udid();

    if (LOCKDOWN_E_SUCCESS != (ldret = lockdownd_client_new(device, &client, "ideviceenterrecovery"))) {
        printf("ERROR: Could not connect to lockdownd, error code %d\n", ldret);
        printf("\n");
        idevice_free(device);
        exit(EXIT_FAILURE);
    }

    if (lockdownd_enter_recovery(client) != LOCKDOWN_E_SUCCESS) {
        printf("ERROR: Failed to enter recovery mode : %s\n", strerror(errno));
        printf("\n");
        exit(-2);
    }

    printf("INFO: iDevice is enterring in recovery mode.\n");

    printf("\n");

    exit(0);
}

void tool_exit_recovery(void) {
    int i = 0;
    irecv_error_t error = 0;
    unsigned long long ecid = 0;

    irecv_init();

    for (i = 0; i <= 5; i++) {
        if (irecv_open_with_ecid(&client, ecid) != IRECV_E_SUCCESS)
            sleep(1);
        else
            break;

        if (i == 5) {
            exit(EXIT_FAILURE);
        }
    }   

    printf("\n");

    error = irecv_setenv(client, "auto-boot", "true");
    if (error != IRECV_E_SUCCESS) {
        printf("%s\n", irecv_strerror(error));
        exit(EXIT_FAILURE);
    }

    error = irecv_saveenv(client);
    if (error != IRECV_E_SUCCESS) {
        printf("%s\n", irecv_strerror(error));
        exit(EXIT_FAILURE);
    }

    error = irecv_reboot(client);
    if (error != IRECV_E_SUCCESS) {
        printf("%s\n", irecv_strerror(error));
    } else {
        printf("%s\n", irecv_strerror(error));
    }

    irecv_close(client);
    irecv_exit();

    printf("iDevice is exiting recovery mode\n");
    printf("\n");

    exit(0);
}


/********** Result of all infos collected **********/
int tool_get_infos() {

    printf("\n");

    tool_get_udid(); // Getting UDID in normal mode,

    tool_get_uuid(); // Generate UUID in normal & recovery mode.

    if (IDEVICE_E_SUCCESS != idevice_new(&device, NULL)) {

        // void again oof..

    } else {

        tool_get_name(); // I can not get in recovery..

        printf("FAIL: → Recovery\n"); 

        printf("\n");

        exit(EXIT_FAILURE);
    }

    tool_get_all(); // Getting everything in recovery mode.

    printf("\n");

    return 0;
}
