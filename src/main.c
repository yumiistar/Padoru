//
//  main.c
//  Padoru
//
//  Created by yumiistar on 11/18/18.
//  Copyright © 2018 yumiistar. All rights reserved.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <Padoru.h>

#define VERSION "version: 2.1"

static struct option longopts[] = {
    { "infos",      no_argument,        NULL, 'i'},
    { "serial",     required_argument,  NULL, 's'},
    { "exit",       no_argument,        NULL, 'e'},
    { "recovery",   no_argument,        NULL, 'r'},
    { "help",       no_argument,        NULL, 'h'},
    { NULL,         0,                  NULL,  0 }
};

void usage(int argc, char *argv[]) {
    char *name = NULL;
    name = strrchr(argv[0], '/');
    printf("\n");
    printf("usage: Padoru [args]\n");
    printf("-e, --exit\t\t\texit recovery mode\n");
    printf("-r, --recovery\t\t\tenter in recovery mode\n");
    printf("-s, --serial [PORT]\t\tspawn a serial terminal\n");
    printf("-i, --infos\t\t\tprint infos about a connected idevice\n");
    printf("\n");
}

int main(int argc, char *argv[]) {

    int opt = 0;
    int optindex = 0;
    int padorupadoru = 0;
    int exit_recovery = 0;
    int enter_recovery = 0;
    int serial_terminal = 0;

    if (argc < 2) {   
        usage(argc, argv);
        return 0;
    }

    while((opt = getopt_long(argc, argv, "iserh", longopts, &optindex)) > 0) {
        switch(opt) {
            case 'i':
                padorupadoru = 1;
                break;
            case 's':
                serial_terminal = 1;
                break;
            case 'e':
                exit_recovery = 1;
                break;
            case 'r':
                enter_recovery = 1;
                break;
            case 'h' :
                usage(argc, argv);
                break;
            default : 
                usage(argc, argv);
        }
    }

    if (padorupadoru) {
        // Giving infos about a connected iDevice
        tool_get_infos();
    }

    if (serial_terminal) {   
        // Spawn a serial terminal
        tool_serial_terminal(argc, argv);
    }

    if (exit_recovery) {   
        // Exiting recovery mode
        tool_exit_recovery();
    }

    if (enter_recovery) {
        // Entering in recovery mode
        tool_enter_recovery();
    }

    return 0;
}
