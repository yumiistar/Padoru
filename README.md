Padoru
<br />
<br />
Padoru was developed by [@yumiistar](https://twitter.com/yumiistar), <br />
This is small tool used to replace PurpleRegister or iTunes for a recovery device. <br />
Using [libimobiledevice](https://github.com/libimobiledevice) (use ideviceinfo for more details..).
<br />

I. Compile: <br />
	`make; sudo make install; make clean`
	
<br />

II. Usage: <br />
```
usage: Padoru [args]:
    -e, --exit 				exit recovery mode\n");
    -r, --recovery 			enter in recovery mode
    -s, --serial [PORT] 		spawn a serial terminal
    -i, --infos 			print infos about a connected idevice
```

<br />

III. Todo: <br />
- Get IMEI & serial no & version infos for a connected device,
- Getting UDID & NAME working for a recovery device.

<br />

IV. Thanks - Credits: <br />
- [matteyeux](https://twitter.com/matteyeux) for help,
- [libimobiledevice](https://github.com/libimobiledevice) for libimobiledevice & libirecovery.

<br />
[NOTE]: <br /> If you start padoru with a device in normal mode, UDID & NAME will be printed.
	For the rest, you need to switch in recovery mode (using "Padoru -r" & exiting using "Padoru -e").
	A fix will comes soon, that is on the todo list.
	