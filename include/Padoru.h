//
//  utilities.h
//  Padoru
//
//  Created by yumiistar on 11/18/18.
//  Copyright © 2018 yumiistar. All rights reserved.
//
//  Padoru Infos Library
//  Created by yumiistar on 12/01/18.
//  Copyright © 2010-2013 libimobiledevice. All Rights Reserved.
//

#ifndef _LIBIPADORU_H_
#define _LIBIPADORU_H_

#include <libirecovery.h>
#include <libimobiledevice/lockdown.h>
#include <libimobiledevice/libimobiledevice.h>

void tool_serial_terminal(int argc, char *argv[]); // Spawn a serial terminal;

void tool_enter_recovery(void); // Enter in recovery mode;

void tool_exit_recovery(void); // Exit in recovery mode;

static void tool_get_uuid(void); // Generate UUID;

static void tool_get_name(void); // Get iDevice name; 

static void tool_get_udid(void); // Get iDevice UDID;

static void tool_get_all(void); // Get iDevice ECID & IMEI & MODE & MODEL & DEVICE;

int tool_get_infos(void); // Print all infos about a connected device;

#endif
