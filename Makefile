CC = gcc
TARGET = Padoru
__arch__ = $(shell arch)
__FILE__ = Padoru.h
__uname_s__ = $(shell uname -s)
LDFLAGS = -Wall -W -limobiledevice -lirecovery

OBJECTS = src/main.o \
		src/Padoru.o

all : padoru

padoru : $(TARGET)

$(TARGET) : $(OBJECTS)
		@echo "[INFO]: Exporting header.."
		rm -f /usr/local/include/$(__FILE__)
		cp include/$(__FILE__) /usr/local/include/
		@echo "OK: Exported header"
			
		@echo "[INFO]: Compiling Padoru.."
		$(CC) $(OBJECTS) $(LDFLAGS) -o $(TARGET)
		@echo "OK: $(TARGET) for $(__uname_s__) $(__arch__)"

install :
		@echo "[INFO]: Exporting Padoru.."
		sudo cp $(TARGET) /usr/local/bin/
		@echo "OK: Padoru binary"
		
clean :
		rm -rf src/*.o $(TARGET)
